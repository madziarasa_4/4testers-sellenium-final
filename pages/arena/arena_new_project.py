from selenium.webdriver.common.by import By


class ArenaNewProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def insert_title(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_text)

    def insert_prefix(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_text)

    def insert_description(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(random_text)

    def save_new_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title
