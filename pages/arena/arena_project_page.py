from selenium.webdriver.common.by import By


class ArenaProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def click_add_new_project_icon(self):
        self.browser.find_element(By.CSS_SELECTOR, '.button_link').click()

    def insert_search_text(self, prefix):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(prefix)

    def click_search(self):
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def check_results(self, prefix):
        results = self.browser.find_elements(By.CSS_SELECTOR, '.t_number')
        r = 0
        for i in results:
            if i.text == prefix:
                r = 1
        return r
