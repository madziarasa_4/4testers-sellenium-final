import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_messages import ArenaMessagesPage
from pages.arena.arena_new_project import ArenaNewProjectPage
from pages.arena.arena_project_page import ArenaProjectPage
from utils.random_message import generate_random_text
from selenium import webdriver

@pytest.fixture
def browser():
    driver = webdriver.Chrome()
    #driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()

def test_should_open_projects_page(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page = ArenaProjectPage(browser)
    #arena_project_page.verify_title('Projekty')


def test_should_add_new_project(browser):
    # sekcja inicjacji stałych tekstowych
    random_title = generate_random_text(10)
    random_prefix = generate_random_text(5)
    random_description = generate_random_text(15)

    # sekcja ładowanie strony z projektami
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page = ArenaProjectPage(browser)

    # Otwieranie strony nowego projektu
    arena_project_page.click_add_new_project_icon()

    # Dodawanie nowego projektu
    arena_new_project_page = ArenaNewProjectPage(browser)
    arena_new_project_page.verify_title('Dodaj projekt')
    arena_new_project_page.insert_title(random_title)
    arena_new_project_page.insert_prefix(random_prefix)
    arena_new_project_page.insert_description(random_description)
    arena_new_project_page.save_new_project()
    # time.sleep(5)

    # Sprawdzenie
    arena_home_page.load_projects_form_left_menu()
    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.insert_search_text(random_prefix)
    # time.sleep(5)
    arena_project_page.click_search()
    # time.sleep(5)
    assert arena_project_page.check_results(random_prefix) == 1
    # time.sleep(5)

